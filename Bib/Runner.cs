﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace Bib
{
    public class Runner
    {
        const int DLL_PROCESS_ATTACH = 0;
        const int DLL_THREAD_ATTACH = 1;
        const int DLL_THREAD_DETACH = 2;
        const int DLL_PROCESS_DETACH = 3;

        [DllExport("Init")]
        public static void Init() { }

        [DllExport("DllMain")]
        public static bool DllMain()
        {
            NativeMethods.AllocConsole();
            NativeMethods.ShowWindow(NativeMethods.GetConsoleWindow(), 5);
            Console.WriteLine("Init.. ");
            Console.WriteLine("DLL_PROCESS_ATTACH");

            Hook.D3DHook hook = new Hook.D3DHook();

            Thread t_game = new Thread(() => Game.Run(hook));
            t_game.Start();

            return true;
        }
    }
}
