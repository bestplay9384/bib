﻿using EasyHook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static Bib.NativeMethods;

namespace Bib.Hook
{
    class Hook22
    {
        public static void Initialize()
        {
            // hook 
            using (var hook = LocalHook.Create(
                    LocalHook.GetProcAddress("gdi32.dll", "BitBlt"),
                    new BitBltDelegate(BitBltHook),
                    null))
            {
                // hook start
                hook.ThreadACL.SetExclusiveACL(new int[] { 0 });

                // hook end
                //hook.ThreadACL.SetExclusiveACL(new int[] { 0 });

            } // hook.Dispose() will uninstall the hook for us
        }

        public static bool BitBltHook([In] IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, [In] IntPtr hdcSrc, int nXSrc, int nYSrc, TernaryRasterOperations dwRop)
        {
            try
            {
                Console.WriteLine("BITBLT CALLED!");
                Game.BitBltCalled = true;
                Task.Delay(25).Wait();
                return NativeMethods.BitBlt(hdc, nXDest, nYDest, nWidth, nHeight, hdcSrc, nXSrc, nYSrc, dwRop);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
