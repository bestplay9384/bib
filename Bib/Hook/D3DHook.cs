﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using SharpDX.Direct3D;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX;

namespace Bib.Hook
{
    internal class D3DHook
    {
        private enum D3D11DeviceVTbl : short
        {
            // IUnknown
            QueryInterface = 0,
            AddRef = 1,
            Release = 2,

            // ID3D11Device
            CreateBuffer = 3,
            CreateTexture1D = 4,
            CreateTexture2D = 5,
            CreateTexture3D = 6,
            CreateShaderResourceView = 7,
            CreateUnorderedAccessView = 8,
            CreateRenderTargetView = 9,
            CreateDepthStencilView = 10,
            CreateInputLayout = 11,
            CreateVertexShader = 12,
            CreateGeometryShader = 13,
            CreateGeometryShaderWithStreamOutput = 14,
            CreatePixelShader = 15,
            CreateHullShader = 16,
            CreateDomainShader = 17,
            CreateComputeShader = 18,
            CreateClassLinkage = 19,
            CreateBlendState = 20,
            CreateDepthStencilState = 21,
            CreateRasterizerState = 22,
            CreateSamplerState = 23,
            CreateQuery = 24,
            CreatePredicate = 25,
            CreateCounter = 26,
            CreateDeferredContext = 27,
            OpenSharedResource = 28,
            CheckFormatSupport = 29,
            CheckMultisampleQualityLevels = 30,
            CheckCounterInfo = 31,
            CheckCounter = 32,
            CheckFeatureSupport = 33,
            GetPrivateData = 34,
            SetPrivateData = 35,
            SetPrivateDataInterface = 36,
            GetFeatureLevel = 37,
            GetCreationFlags = 38,
            GetDeviceRemovedReason = 39,
            GetImmediateContext = 40,
            SetExceptionMode = 41,
            GetExceptionMode = 42,
        }

        private const int D3D11_DEVICE_METHOD_COUNT = 43;

        private enum DXGISwapChainVTbl : short
        {
            // IUnknown
            QueryInterface = 0,
            AddRef = 1,
            Release = 2,

            // IDXGIObject
            SetPrivateData = 3,
            SetPrivateDataInterface = 4,
            GetPrivateData = 5,
            GetParent = 6,

            // IDXGIDeviceSubObject
            GetDevice = 7,

            // IDXGISwapChain
            Present = 8,
            GetBuffer = 9,
            SetFullscreenState = 10,
            GetFullscreenState = 11,
            GetDesc = 12,
            ResizeBuffers = 13,
            ResizeTarget = 14,
            GetContainingOutput = 15,
            GetFrameStatistics = 16,
            GetLastPresentCount = 17,
        }

        private const int DXGI_SWAPCHAIN_METHOD_COUNT = 18;

        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Unicode, SetLastError = true)]
        private delegate int DXGISwapChain_PresentDelegate(IntPtr swapChainPtr, int syncInterval, /* int */ SharpDX.DXGI.PresentFlags flags);

        [UnmanagedFunctionPointer(CallingConvention.StdCall, CharSet = CharSet.Unicode, SetLastError = true)]
        delegate int DXGISwapChain_ResizeTargetDelegate(IntPtr swapChainPtr, ref ModeDescription newTargetParameters);

        private List<IntPtr> _d3d11VTblAddresses = null;
        private List<IntPtr> _dxgiSwapChainVTblAddresses = null;

        private Hook<DXGISwapChain_PresentDelegate> DXGISwapChain_PresentHook = null;
        private Hook<DXGISwapChain_ResizeTargetDelegate> DXGISwapChain_ResizeTargetHook = null;

        SharpDX.Direct3D11.Device _device;
        SharpDX.DXGI.SwapChain _swapChain;

        Capture.Hook.DX11.DXOverlayEngine _overlayEngine;
        IntPtr _swapChainPointer = IntPtr.Zero;

        private static SharpDX.DXGI.SwapChainDescription CreateSwapChainDescription(IntPtr windowHandle)
        {
            return new SharpDX.DXGI.SwapChainDescription
            {
                BufferCount = 1,
                Flags = SharpDX.DXGI.SwapChainFlags.None,
                IsWindowed = true,
                ModeDescription = new SharpDX.DXGI.ModeDescription(100, 100, new Rational(60, 1), SharpDX.DXGI.Format.R8G8B8A8_UNorm),
                OutputHandle = windowHandle,
                SampleDescription = new SharpDX.DXGI.SampleDescription(1, 0),
                SwapEffect = SharpDX.DXGI.SwapEffect.Discard,
                Usage = SharpDX.DXGI.Usage.RenderTargetOutput
            };
        }

        private static IntPtr[] GetVTblAddresses(IntPtr pointer, int numberOfMethods)
        {
            List<IntPtr> vtblAddresses = new List<IntPtr>();

            IntPtr vTable = Marshal.ReadIntPtr(pointer);
            for (int i = 0; i < numberOfMethods; i++)
                vtblAddresses.Add(Marshal.ReadIntPtr(vTable, i * IntPtr.Size)); // using IntPtr.Size allows us to support both 32 and 64-bit processes

            return vtblAddresses.ToArray();
        }

        public void Hook()
        {
            Console.WriteLine("Hooking D3D...");
            if (this._d3d11VTblAddresses == null)
            {
                this._d3d11VTblAddresses = new List<IntPtr>();
                this._dxgiSwapChainVTblAddresses = new List<IntPtr>();

                SharpDX.Windows.RenderForm _renderForm = new SharpDX.Windows.RenderForm();

                SharpDX.Direct3D11.Device.CreateWithSwapChain(
                        DriverType.Hardware,
                        DeviceCreationFlags.BgraSupport,
                        CreateSwapChainDescription(_renderForm.Handle),
                        out this._device,
                        out this._swapChain);


                var d2dFactory = new SharpDX.Direct2D1.Factory();


                if (_device != null && _swapChain != null)
                {
                    Console.WriteLine("Hook: Device created.");
                    this._d3d11VTblAddresses.AddRange(GetVTblAddresses(this._device.NativePointer, D3D11_DEVICE_METHOD_COUNT));
                    this._dxgiSwapChainVTblAddresses.AddRange(GetVTblAddresses(this._swapChain.NativePointer, DXGI_SWAPCHAIN_METHOD_COUNT));
                }
                else
                {
                    Console.WriteLine("Hook: Device creation failed.");
                }
            }

            this.DXGISwapChain_PresentHook = new Hook<DXGISwapChain_PresentDelegate>(
                this._dxgiSwapChainVTblAddresses[(int)DXGISwapChainVTbl.Present],
                new DXGISwapChain_PresentDelegate(this.PresentHook),
            this);

            this.DXGISwapChain_PresentHook.Enable();

            this.DXGISwapChain_ResizeTargetHook = new Hook<DXGISwapChain_ResizeTargetDelegate>(
                _dxgiSwapChainVTblAddresses[(int)DXGISwapChainVTbl.ResizeTarget],
                new DXGISwapChain_ResizeTargetDelegate(ResizeTargetHook),
                this);

            this.DXGISwapChain_ResizeTargetHook.Enable();

            Console.WriteLine("Hook: Hook is in place.");
        }

        /** HOOKED METHODS **/
        public int ResizeTargetHook(IntPtr swapChainPtr, ref ModeDescription newTargetParameters)
        {
            Console.WriteLine("ResizeTargetHook");
            SwapChain swapChain = (SharpDX.DXGI.SwapChain)swapChainPtr;
            // Dispose of overlay engine (so it will be recreated with correct renderTarget view size)
            if (_overlayEngine != null)
            {
                _overlayEngine.Dispose();
                _overlayEngine = null;
            }

            swapChain.ResizeTarget(ref newTargetParameters); 
            return DXGISwapChain_ResizeTargetHook.Original(swapChainPtr, ref newTargetParameters);
        }

        public int PresentHook(IntPtr swapChainPtr, int syncInterval, SharpDX.DXGI.PresentFlags flags)
        {
            Console.WriteLine("PresentHook");
            SharpDX.DXGI.SwapChain swapChain = (SharpDX.DXGI.SwapChain)swapChainPtr;

            try
            {
                // Initialise Overlay Engine
                if (_swapChainPointer != swapChain.NativePointer || _overlayEngine == null)
                {
                    if (_overlayEngine != null)
                        _overlayEngine.Dispose();

                    _overlayEngine = new Capture.Hook.DX11.DXOverlayEngine();
                    _overlayEngine.Overlays.Add(new Capture.Hook.Common.Overlay
                    {
                        Elements =
                            {
                                new Capture.Hook.Common.FramesPerSecond(new System.Drawing.Font("Arial", 16)) { Location = new System.Drawing.Point(5,5), Color = System.Drawing.Color.Red, AntiAliased = true },
                                new Capture.Hook.Common.TextElement(new System.Drawing.Font("Times New Roman", 22)) { Text = "Test", Location = new System.Drawing.Point(200, 200), Color = System.Drawing.Color.Yellow, AntiAliased = true},
                            }
                    });
                    _overlayEngine.Initialise(swapChain);

                    _swapChainPointer = swapChain.NativePointer;
                }
                // Draw Overlay(s)
                else if (_overlayEngine != null)
                {
                    foreach (var overlay in _overlayEngine.Overlays)
                        overlay.Frame();
                    _overlayEngine.Draw();
                }
            } catch (Exception e)
            {
                Console.WriteLine("PresentHook: Exeception: " + e.GetType().FullName + ": " + e.ToString());
            }

            return this.DXGISwapChain_PresentHook.Original(swapChainPtr, syncInterval, flags);
        }



        
    }
}
