﻿using Process.NET;
using Process.NET.Memory;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bib
{
    class Game
    {
        public static bool BitBltCalled = false;
        public static ProcessSharp ProcessSharp { get; set; }

        public static bool IsValid(Int64 val)
        {
            return (val >= 0x10000 && val < 0x000F000000000000);
        }

        public static void Run(Hook.D3DHook render)
        {
            ProcessSharp = new ProcessSharp(System.Diagnostics.Process.GetCurrentProcess(), MemoryType.Local);
            ProcessSharp.Memory = new ExternalProcessMemory(ProcessSharp.Handle);
            render.Hook();

            while (true)
            {
                Int64 pGContext = (long)ProcessSharp.Memory.Read<UInt64>((IntPtr)Offsets.CLIENTGAMECONTEXT);
                if (!IsValid(pGContext))
                    continue;

                Int64 pPlayerManager = ProcessSharp.Memory.Read<Int64>((IntPtr)(pGContext + Offsets.PlayerManager));
                if (!IsValid(pPlayerManager))
                    continue;

                Int64 m_ppPlayer = ProcessSharp.Memory.Read<Int64>((IntPtr)(pPlayerManager + Offsets.EnemyPlayer));
                if (!IsValid(m_ppPlayer))
                    continue;

                Int64 pLocalPlayer = ProcessSharp.Memory.Read<Int64>((IntPtr)(pPlayerManager + Offsets.LocalPlayer));
                if (!IsValid(pLocalPlayer))
                    continue;

                Int64 pLocalSoldier = ProcessSharp.Memory.Read<Int64>((IntPtr)(pLocalPlayer + Offsets.Player.ClientSoldierEntity));
                if (!IsValid(pLocalSoldier))
                    continue;

                Int64 pLocalPlayerTeam = ProcessSharp.Memory.Read<Int64>((IntPtr)(pLocalPlayer + Offsets.Player.Team));

                for (uint i = 0; i < 60; i++)
                {
                    Int64 pEnemyPlayer = ProcessSharp.Memory.Read<Int64>((IntPtr)(m_ppPlayer + (sizeof(Int64) * i)));
                    if (!IsValid(pEnemyPlayer))
                        continue;

                    Int64 pEnemySoldier = ProcessSharp.Memory.Read<Int64>((IntPtr)(pEnemyPlayer + Offsets.Player.ClientSoldierEntity));
                    if (!IsValid(pEnemySoldier))
                        continue;

                    Int64 pEnemyHealthComponent = ProcessSharp.Memory.Read<Int64>((IntPtr)(pEnemySoldier + Offsets.Player.ClientHealthComponent));
                    if (!IsValid(pEnemyHealthComponent))
                        continue;

                    bool pEnemyOccluded = ProcessSharp.Memory.Read<bool>((IntPtr)(pEnemySoldier + Offsets.Player.isOccluded));
                    float hp = ProcessSharp.Memory.Read<float>((IntPtr)(pEnemyHealthComponent + Offsets.Health.hp));
                    Int64 pEnemyPlayerTeam = ProcessSharp.Memory.Read<Int64>((IntPtr)(pEnemyPlayer + Offsets.Player.Team));

                    //Console.WriteLine("Occcluded: " + pEnemyOccluded + " <> HP: " + hp + " <> Team: " + pEnemyPlayerTeam);

                    // only enemies
                    if (pEnemyPlayerTeam != pLocalPlayerTeam && hp >= 1.0f && hp <= 100.0f)
                    {
                        if (BitBltCalled)
                        {
                            // disable
                            ProcessSharp.Memory.Write<byte>((IntPtr)(pEnemySoldier + Offsets.Player.EngineChams), (!pEnemyOccluded) ? Offsets.ChamsColor.red : Offsets.ChamsColor.original);
                            Thread.Sleep(5000);
                            Game.BitBltCalled = false;
                        } else
                        {
                            ProcessSharp.Memory.Write<byte>((IntPtr)(pEnemySoldier + Offsets.Player.EngineChams), (!pEnemyOccluded) ? Offsets.ChamsColor.red : Offsets.ChamsColor.green);
                        }
                    }
                }
            }
        }
    }
}
