﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bib
{
    class Offsets
    {
        public static string ProcessName = "bf1";
        public static Int64 CLIENTGAMECONTEXT = 0x143417090;
        public static Int64 DXRENDERER = 0x143607018;
        public static Int64 PlayerManager = 0x68;
        public static Int64 LocalPlayer = 0x0578;
        public static Int64 EnemyPlayer = 0x100;

        public struct Player
        {
            public static Int64 ClientSoldierEntity = 0x1D48;
            public static Int64 Team = 0x1C34;
            public static Int64 EngineChams = 0x2FC;
            public static Int64 isOccluded = 0x06AB;
            public static Int64 ClientHealthComponent = 0x01C0;
        }

        public struct Health
        {
            public static Int64 hp = 0x20;
        }

        public struct ChamsColor
        {
            public static byte original = 0;
            public static byte red = 241;
            public static byte green = 240;
        }
    }
}
